package com.vaadinongrails.services

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

class User implements UserDetails {

    String username
    String password

    static constraints = {
    }

    @Override
    Collection<? extends GrantedAuthority> getAuthorities() {
        // TODO:
        return [new SimpleGrantedAuthority("ADMIN"), new SimpleGrantedAuthority("USER")]
    }

    @Override
    boolean isAccountNonExpired() {
        // TODO
        return true
    }

    @Override
    boolean isAccountNonLocked() {
        // TODO
        return true
    }

    @Override
    boolean isCredentialsNonExpired() {
        // TODO
        return true
    }

    @Override
    boolean isEnabled() {
        // TODO
        return true
    }
}
