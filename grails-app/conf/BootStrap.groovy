import com.vaadinongrails.services.User

class BootStrap {

    def init = { servletContext ->
        User user = new User()
        user.username = "john"
        user.password = "john"
        user.save(failOnError: true)
    }

    def destroy = {
    }
}
