import app.security.AuthManager
import app.util.GrailsDataSource

beans = {
    authenticationManager(AuthManager)

    dataSource(GrailsDataSource)
}
