package app

import app.security.SecuredViewChangeListener
import app.security.ViewSecurity
import app.views.home.HomeView
import app.views.items.ItemsView
import app.views.login.LoginView
import app.views.secured.SecuredView
import com.vaadin.annotations.PreserveOnRefresh
import com.vaadin.navigator.Navigator
import com.vaadin.server.VaadinRequest
import com.vaadin.ui.UI
import com.vaadin.ui.VerticalLayout

@PreserveOnRefresh
class MyUI extends UI {

    @Override
    protected void init(VaadinRequest request) {

        VerticalLayout layout = new VerticalLayout()

        Navigator navigator = new Navigator(this, this)

        SecuredViewChangeListener securedViewListener = new SecuredViewChangeListener()
        navigator.addViewChangeListener(securedViewListener)

        navigator.addView(HomeView.VIEW_NAME, HomeView)
        navigator.addView(ItemsView.VIEW_NAME, ItemsView)
        navigator.addView(LoginView.VIEW_NAME, LoginView)
        navigator.addView(SecuredView.VIEW_NAME, SecuredView)

        ViewSecurity.add(HomeView, null)
        ViewSecurity.add(ItemsView, null)
        ViewSecurity.add(LoginView, null)
        ViewSecurity.add(SecuredView, ['ADMIN'])

        navigator.navigateTo(HomeView.VIEW_NAME);

        setContent(layout)
    }
}
