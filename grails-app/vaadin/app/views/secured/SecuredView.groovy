package app.views.secured

import com.vaadin.navigator.View
import com.vaadin.navigator.ViewChangeListener
import com.vaadin.ui.Label
import com.vaadin.ui.VerticalLayout

class SecuredView extends VerticalLayout implements View {

    static final String VIEW_NAME = "secured"

    @Override
    void enter(ViewChangeListener.ViewChangeEvent event) {
        addComponent(new Label("SECURED CONTENT ONLY FOR Admins!"))
    }
}
