package app.views.items

import com.vaadin.grails.Grails
import com.vaadin.ui.Button
import com.vaadinongrails.services.ItemService

import static com.vaadin.ui.UI.getCurrent

class DeleteItemsListener implements Button.ClickListener {

    @Override
    void buttonClick(Button.ClickEvent clickEvent) {
        ItemService itemService = Grails.get(ItemService)

        itemService.deleteAll()

        current.navigator.navigateTo(ItemsView.VIEW_NAME)
    }
}
