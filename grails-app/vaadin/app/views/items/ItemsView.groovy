package app.views.items

import app.views.home.BackHomeListener
import com.vaadin.grails.Grails
import com.vaadin.navigator.View
import com.vaadin.navigator.ViewChangeListener
import com.vaadin.ui.Button
import com.vaadin.ui.Label
import com.vaadin.ui.VerticalLayout
import com.vaadinongrails.services.Item
import com.vaadinongrails.services.ItemService

class ItemsView extends VerticalLayout implements View {

    static final String VIEW_NAME = "items"

    @Override
    void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

        // add one more item
        Button btnAddMoreItems = new Button("Add One More Item")
        btnAddMoreItems.addClickListener(new AddItemListener())
        addComponent(btnAddMoreItems)

        // remove all items
        Button btnDeleteAllItems = new Button("Delete All Items")
        btnDeleteAllItems.addClickListener(new DeleteItemsListener())
        addComponent(btnDeleteAllItems)

        // list all items (this would go to a component that would be responsible for rendering an item)
        ItemService itemService = Grails.get(ItemService)
        List<Item> items = itemService.findAll()
        for (Item item : items) {
            addComponent(new Label(item.name))
        }

        // add back button
        Button btnBack = new Button("Back Home")
        btnBack.addClickListener(new BackHomeListener())
        addComponent(btnBack)

    }
}
