package app.views.items

import com.vaadin.grails.Grails
import com.vaadin.ui.Button
import com.vaadinongrails.services.ItemService

import static com.vaadin.ui.UI.getCurrent

class AddItemListener implements Button.ClickListener {

    @Override
    void buttonClick(Button.ClickEvent clickEvent) {
        ItemService itemService = Grails.get(ItemService)

        itemService.save("An item: " + UUID.randomUUID().toString())

        current.navigator.navigateTo(ItemsView.VIEW_NAME)
    }
}
