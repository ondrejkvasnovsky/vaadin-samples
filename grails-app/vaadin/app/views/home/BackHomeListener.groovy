package app.views.home

import com.vaadin.ui.Button

import static com.vaadin.ui.UI.getCurrent

class BackHomeListener implements Button.ClickListener {

    @Override
    void buttonClick(Button.ClickEvent clickEvent) {
        current.navigator.navigateTo(HomeView.VIEW_NAME)
    }
}
