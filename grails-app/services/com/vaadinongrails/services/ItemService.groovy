package com.vaadinongrails.services

import grails.transaction.Transactional

@Transactional
class ItemService {

    List<Item> findAll() {
        List<Item> result = Item.findAll()
        return result
    }

    Item save(String name) {
        Item item = new Item()
        item.name = name

        item.save(failOnError: true)

        return item
    }

    void deleteAll() {
        // never do this... this is just shortcut to delete all items
        Item.deleteAll(Item.findAll())
    }
}
